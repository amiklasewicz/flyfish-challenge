import axios from 'axios';
require('dotenv').config();


export const getPetById = async (
    petId,
    apiKey
) => {
    let response;
    try {
        response = await axios.get(
            `${process.env.BASE_URL}${process.env.API_VERSION}/pet/${petId}`,
            {
                headers: {
                    api_key: apiKey
                }
            }
        )
        return response;
    } catch (error) {
        console.log(
            `Error occurred while checking petId: ${error}`,
        );
        return error.response;
    }
};
