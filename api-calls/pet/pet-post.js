import axios from 'axios';
require('dotenv').config();

export const addNewPetToTheStore = async (
    payload = {},
    apiKey
  ) => {
    let response;
    try {
        response = await axios.post(
            `${process.env.BASE_URL}${process.env.API_VERSION}/pet`,
            payload,{
            headers:{
                api_key: apiKey
            }
        }
        )
        return response;
    } catch (error) {
        console.log(
            `Error occurred while adding new pet to the store: ${error}`,
        );
        return error.response;
    }
};
