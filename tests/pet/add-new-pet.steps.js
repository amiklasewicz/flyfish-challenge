import { addNewPetToTheStore } from '../../api-calls/pet/pet-post';
import { getPetById } from '../../api-calls/pet/pet-get';
import { checkServiceAvailability } from '../../helpers/requests';
import { defineFeature, loadFeature } from 'jest-cucumber';
import { petPayload, petPayloadCategoryRemoved } from '../../data/pet/pet-data';

const feature = loadFeature('features/pet/add-new-pet.feature');

defineFeature(feature, (test) => {

    beforeEach(async () => {
        let serviceAvailabilityResponse = await checkServiceAvailability();
        expect(serviceAvailabilityResponse.status).toEqual(200);
      });
    
    test(`Add new pet to the store`, ({ given, when, then }) => {
        var payload = petPayload();
        var newPetId;
        let addNewPetToStoreResponse;
        let getPetByIdResponse;

        given(`I add a new pet to the pet store`, async () => {
            addNewPetToStoreResponse = await addNewPetToTheStore(
                payload,
                process.env.API_KEY
            );
        });

        then(`I expect to receive 200 status and proper header`, async () => {
            expect(addNewPetToStoreResponse.status).toEqual(200);
            expect(addNewPetToStoreResponse.headers['content-type']).toContain('application/json')
        });

        then(`I expect POST response to contain proper pet name`, async () => {
            expect(addNewPetToStoreResponse.data.name).toContain(payload.name);
        });

        then('I assign new pet id to variable', async () => {
            newPetId = addNewPetToStoreResponse.data.id;
        });
        
        when(`I verify that pet has been added`, async () => {
            getPetByIdResponse = await getPetById(
                newPetId,
                process.env.API_KEY
            );
        });

        then('I expect to receive 200 status and assigned name same as in payload', async () => {
            expect(getPetByIdResponse.status).toEqual(200);
            expect(getPetByIdResponse.data.name).toContain(payload.name);
        });
    });

    test(`Add new pet to the store with category removed`, ({ given, when, then }) => {
        var payload = petPayloadCategoryRemoved();
        var newPetId;
        let addNewPetToStoreResponse;
        let getPetByIdResponse;

        given(`I add a new pet to the pet store with category removed`, async () => {
            addNewPetToStoreResponse = await addNewPetToTheStore(
                payload,
                process.env.API_KEY
            );
        });

        then(`Then I expect to receive 200 staus as well as proper content header`, async () => {
            expect(addNewPetToStoreResponse.status).toEqual(200);
            expect(addNewPetToStoreResponse.headers['content-type']).toContain('application/json');
        });

        then(`Then I assign new pet id to variable`, async () => {
            newPetId = addNewPetToStoreResponse.data.id;
        });

        when(`I verify that category has been removed`, async () => {
            getPetByIdResponse = await getPetById(newPetId);
        });

        then(`Then I can see added pet in the store without category`, async () => {
            expect(getPetByIdResponse.status).toEqual(200);
            expect(getPetByIdResponse.data.category).toBe(undefined);
        });
    });

    test(`Add new pet to the store with edge case name`, ({ given, when, then }) => {
        var payload = petPayload();
        payload.name = String.fromCharCode(140, 156, 67);
        var newPetId;
        let addNewPetToStoreResponse;
        let getPetByIdResponse;

        given(`I add a new pet to the pet store with edge case name`, async () => {
            addNewPetToStoreResponse = await addNewPetToTheStore(
                payload,
                process.env.API_KEY
            );
        });

        then(`Then I expect to receive 200 staus as well as proper content header`, async () => {
            expect(addNewPetToStoreResponse.status).toEqual(200);
            expect(addNewPetToStoreResponse.headers['content-type']).toContain('application/json');
        });

        then(`Then I assign new pet id to variable`, async () => {
            newPetId = addNewPetToStoreResponse.data.id;
        });

        when(`I verify that name has been properly assigned`, async () => {
            getPetByIdResponse = await getPetById(
                newPetId,
                process.env.API_KEY
            );
        });

        then(`Then I expect to receive 200 status and assigned name same as in payload`, async () => {
            expect(getPetByIdResponse.status).toEqual(200);
            expect(getPetByIdResponse.data.name).toContain(payload.name);
        });
    });

    test(`Add new pet to the store with wrong id`, ({ given, then }) => {
        var payload = petPayload();
        payload.id = "r";
        let addNewPetToStoreResponse;

        given(`I add a new pet to the pet store with wrong id`, async () => {
            addNewPetToStoreResponse = await addNewPetToTheStore(
                payload,
                process.env.API_KEY
            );
        });
        then('I expect to receive 50x status and meaningful error message', async () => {
            expect(addNewPetToStoreResponse.status).toEqual(500);
            expect(addNewPetToStoreResponse.statusText).toContain('Server Error');

        });
    });

    test(`Add new pet to the store with empty schema`, ({ given, then }) => {
        var payload = null;
        let addNewPetToStoreResponse;

        given(`I add a new pet to the pet store with empty schema`, async () => {
            addNewPetToStoreResponse = await addNewPetToTheStore(
                payload,
                process.env.API_KEY
            );
        });
        then('I expect to receive 40x status and meaningful error message', async () => {
            expect(addNewPetToStoreResponse.status).toEqual(415);
            expect(addNewPetToStoreResponse.statusText).toContain('Unsupported Media Type');

        });
    });
});
