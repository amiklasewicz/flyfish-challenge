Feature: Add new pet to the store

    Scenario: Add new pet to the store
        Given I add a new pet to the pet store
        Then I expect to receive 200 status and proper header
        Then I expect POST response to contain proper pet name
        Then I assign new pet id to variable
        When I verify that pet has been added
        Then I expect to receive 200 status and assigned name same as in payload

    Scenario: Add new pet to the store with category removed
        Given I add a new pet to the pet store with category removed
        Then Then I expect to receive 200 staus as well as proper content header
        Then Then I assign new pet id to variable
        When I verify that category has been removed
        Then Then I can see added pet in the store without category

    Scenario: Add new pet to the store with edge case name
        Given I add a new pet to the pet store with edge case name
        Then Then I expect to receive 200 staus as well as proper content header
        Then Then I assign new pet id to variable
        When I verify that name has been properly assigned
        Then Then I expect to receive 200 status and assigned name same as in payload

    Scenario: Add new pet to the store with wrong id
        Given I add a new pet to the pet store with wrong id
        Then I expect to receive 50x status and meaningful error message

    Scenario: Add new pet to the store with empty schema
        Given I add a new pet to the pet store with empty schema
        Then I expect to receive 40x status and meaningful error message
