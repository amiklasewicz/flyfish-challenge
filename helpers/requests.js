import axios from 'axios';
require('dotenv').config();

 async function checkServiceAvailability() {
    let response;
    try {
      response = await axios.get(
        `${process.env.BASE_URL}`
      );
      return response;
    } catch (error) {
      console.log(
        `Error occurred while checking service availability ${error}`,
      );
      return error.response;
    }
  };

  module.exports = {
    checkServiceAvailability
    
};