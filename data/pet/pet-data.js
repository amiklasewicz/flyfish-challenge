import { faker } from '@faker-js/faker';

export const petPayload = () => {
    const payload = {
        id: faker.number.int({ min: 21321321, max: 912902213123 }),
        category: {
            id: faker.number.int({ min: 1, max: 20 }),
            name: "pets",
        },
        name: faker.person.firstName(),
        photoUrls: ['https://www.pexels.com/photo/brown-pomeranian-puppy-on-grey-concrete-floor-3687770/'],
        tags: [{
            id: faker.number.int({ min: 30, max: 40 }),
            name: faker.person.firstName(),
        },
        ],
        status: "Available"
    };
    return payload;
};

export const petPayloadCategoryRemoved = () => {
    const payload = {
        id: faker.number.int({ min: 21321321, max: 912902213123 }),
        name: faker.person.firstName(),
        photoUrls: ['https://www.pexels.com/photo/brown-pomeranian-puppy-on-grey-concrete-floor-3687770/'],
        tags: [{
            id: faker.number.int({ min: 30, max: 40 }),
            name: faker.person.firstName(),
        },
        ],
        status: "Available"
    };
    return payload;
};
