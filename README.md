# FlyFish Challenge

The following code implements FlyFish's challenge of testing the /pet endpoint of a pet store.

## Tech stack
Jest - Framework for API assertions  
Axios - Library for requests sending  
Babel - Library to convert imports into require  
Jest-html-reporter - Reporting service  
Faker - Data Generator  
Jest-cucumber - BDD implementation  
dotenv - .env config to store  

## Installation

Install node.js - https://nodejs.org/en

```bash 
# install all dependencies
npm i 
```
![alt text](img/npmi.gif)

## Usage

```bash 
# starts the tests
npm test 
```
![alt text](img/npmtest.gif)

## Reporting
After every run, a file test-report.html is generated where reports can be checked
![alt text](img/testReport.png)

## Architecture

* [api-calls] - **All functions for API calls**
   * [pet]
     * [pet-get.js]
     * [pet-post.js]
 * [data] - **All files for data management**
   * [pet]
     * [pet-data.js]
 * [features] - **All BDD scenarios**
   * [pet]
     * [add-new-pet.feature]
 * [helpers] - **Functions that are general and not assigned to any test directly**
    * [requests.js]
* [tests] - **All tests that are executed**
   * [pet]
     * [add-new-pet-steps.js]
 * [.env] - **Globally available variables (URL, API key, etc..)**
 * [babel.config.js] - **Config for babel(import alterations)**
 * [jest.config.js] - **Config for Jest(Runners, reporters, etc)**
 * [package.json] - **All libraries and dependencies**

The overall architecture has been divided into 5 layers  
* Functions related to API  
* Data management  
* BDD management  
* Helpers  
* Tests
Personally find this type of architecture the most suitable so I can manage every aspect of created solution

## Pseudocode
```bash 
# Scenario 1
# Add a new pet to the store
 payload = define payload according to /pet schema
 apiKey - pass an API key  

execute GET method on service to check if available
GET(petStoreUrlUrl, apiKey)
    expect(to authorize properly)
    expect(to receive 200 response)
if all conditions are met 
  continue

Execute POST on /pet endpoint and pass payload
POST(petStoreUrlUrl/pet, payload, apiKey)
   expect(to authorize properly)
   expect(to receive 200)
   expect(header to be proper)
   expect(attributes in response are the same as in payload)
return id of the created item
if all conditions are met 
 continue

execute GET on the newly created item
GET(id of newly created item, apiKey)
     expect(to authorize properly)
     expect(to receive 200)
     expect(attributes in response are the same as in payload)
if all conditions met
 finish test as passed
```
```bash 
# Scenario 2
# Add a new pet to the store without category schema
 payloadWithoutCategory = removed category from schema
 apiKey - pass an API key  

execute GET method on service to check if available
GET(petStoreUrlUrl, apiKey)
    expect(to authorize properly)
    expect(to receive 200 response)
if all conditions are met 
  continue

Execute POST on /pet endpoint and pass payload without category schema
POST(petStoreUrlUrl/pet, payloadWithoutCategory, apiKey)
   expect(to authorize properly)
   expect(to receive 200)
   expect(header to be proper)
   expect(attributes in response are the same as in payload)
return id of the created item
if all conditions are met 
 continue

execute GET on the newly created item
GET(id of newly created item, apiKey)
     expect(to authorize properly)
     expect(to receive 200)
     expect(category not present in schema)
if all conditions met
 finish test as passed
```
```bash 
# Scenario 3
# Add a new pet to the store with an edge case name
 payload = default payload
 payload.name = modify name to ASCII signs
 apiKey - pass an API key  

execute GET method on service to check if available
GET(petStoreUrlUrl, apiKey)
    expect(to authorize properly)
    expect(to receive 200 response)
if all conditions are met 
  continue

Execute POST on /pet endpoint and pass payload edge case name
POST(petStoreUrlUrl/pet, payload, apiKey)
   expect(to authorize properly)
   expect(to receive 200)
   expect(header to be proper)
return id of the created item
if all conditions are met 
 continue

execute GET on the newly created item
GET(id of newly created item, apiKey)
     expect(to authorize properly)
     expect(to receive 200)
     expect(Edge case name to be returned and visible)
if all conditions met
 finish test as passed
```
```bash 
# Scenario 4
# Add a new pet to the store with the wrong id
 payload = default payload
 payload.id = define letter instead of number
 apiKey - pass an API key  

execute GET method on service to check if available
GET(petStoreUrl, apiKey)
    expect(to authorize properly)
    expect(to receive 200 response)
if all conditions are met 
  continue

Execute POST on /pet endpoint and pass payload with the wrong id
POST(petStoreUrl/pet, payload, apiKey)
   expect(to authorize properly)
   expect(to receive 500)
   expect(error message text to be meaningful)
if all conditions are met 
 finish test as passed
```

```bash 
# Scenario 5
# Add a new pet to the store without the schema
 payload = value set to 0
 apiKey - pass an API key  

execute GET method on service to check if available
GET(petStoreUrl, apiKey)
    expect(to authorize properly)
    expect(to receive 200 response)
if all conditions are met 
  continue

Execute POST on /pet endpoint and pass payload without schema
POST(petStoreUrl/pet, payload, apiKey)
   expect(to authorize properly)
   expect(to receive 415)
   expect(error message text to be meaningful)
if all conditions are met 
 finish test as passed
```
## Comments and TODOs
As we discussed this challenge should take up to 2 hours, so I didn't want to go too far with tests, I have created 3 positive and 2 negative scenarios. Given more time I would introduce the following things:
* Better data management - Currently I have 2 sets of data that rely partially on hardcoded value and partially on faker
* Parametrize all assertions, like error messages etc.
* Move all functions that are repeatable into beforeAll or beforeEach function
* Tests would be more comprehensive and check more things for example response time, schema validation, and field types validation.
* I am a fan of the data-driven approach and I feel limited by cucumber in that matter, thus there is a possibility I would discuss further usage of cucumber
* Despite covering all the endpoints I would put a weight on security testing and introduce contract-based testing to make sure isolated services talk to each other
* Introduce better reporting with hosting somewhere for example AWS so we can see the history
* Introduce metrics tracking
* Introduce CI/CD
* Pet-store API comments:  
No Id validation that results in neccesity to create a custom approach that will check it every time  
You can remove almost whole schema and there is no validation  
API does not check the validity of provided link
Authorization is always positive no matter what API key we set(even with special-key)
